$(function() { //ready function 
	var offset = 300,
	//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
	offset_opacity = 1200,
	//duration of the top scrolling animation (in ms)
	scroll_top_duration = 700,
	//grab the "back to top" link
	$back_to_top = $('.cd-top');
	
	//GOTO TOP
	$(".filter").removeClass('filter-scroll');
	$('.side-nav').css('margin-top', '89px');
	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});
	var search_flag = 0;
	$(document).on('scroll', function() {
		
		if ($(window).scrollTop() > 10) {
			$('nav').css('top', '0');
			$('.side-nav').css('margin-top', '64px');
		} else {
			$('nav').css('top', '25px');
			$('.side-nav').css('margin-top', '89px');
		}
		
		if ($(window).scrollTop() > 300) {
			$(".filter").addClass('filter-scroll');
			$(".filter-by input.select-dropdown ").css("border-bottom", "none");
		} else {
			$(".filter").removeClass('filter-scroll');
			$(".filter-by input.select-dropdown ").css("border-bottom", "1px solid #9e9e9e");
		}
		
	});
	
	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
	
});