<div class="modal-content">
    	<small>Halaman masuk, pastikan akun email dan password anda tidak diberikan kepada orang lain. <a href="#" class="daftar">Daftar</a> | <a href="#" class="daftar">Lupa kata sandi ?</a></small>
    </div>
    <div class="row">
	    <form class="col s7" style="padding-left: 40px;">
	    	<div class="row">
	        	<div class="input-field col s12">
	          		<input id="email" type="email" class="validate">
	          		<label for="email">Email</label>
	        	</div>
	      	</div>
	    	<div class="row">
	        	<div class="input-field col s12">
	          		<input id="password" type="password" class="validate">
	          		<label for="password">Password</label>
	        	</div>
	      	</div>
		   
		    <div class="row" style="padding-left: 10px;">
		      <button class="btn btn-masuk waves-effect waves-light blue darken-2" style="margin-right: 10px;">Masuk</button> 
		      
		    </div>
		   
	    </form>
	   	<div class="col s5" >
	    	<div class="row">
	    		<small>Masuk menggunakan : </small>
	    	</div>
	    	<div class="row" style="padding-left: 40px; ">
	    		<button class="btn btn-facebook waves-effect waves-light" style="width: 150px;">Facebook</button>
	    	</div>
	    	<div class="row" style="padding-left: 40px; ">
	    		<button class="btn btn-google waves-effect waves-light" style="width: 150px;">Google+</button>
	    	</div>
	    </div>
	    
    </div>