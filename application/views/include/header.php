<header>
	
	<div class="navbar-fixed">
		<div class="top-menu">
			<div class="top-menu-content">
				berpartisipasi untuk membangun ukm indonesia
			</div>
			<div class="top-menu-close">
				
				<!-- <a href="#" class="close_top">Close</a> -->
			</div>
		</div>
		<nav>
			<ul class="left hide-on-med-and-down circle">
				<li><a href="#!" class="btn-floating sideNav waves-effect waves-circle waves-light"><i class="small mdi-navigation-menu"></i></a></li>
			</ul>
			<!-- <a href="#" class="brand-logo "><img style="padding-top: 10px; width: 200px; padding-left: 10px;"  src="<?php echo $this->config->item('img')."logo-small.png";?>"></a> -->
			
			<ul class="right" style="padding-right: 10px;">
				<li><a data-target="modal1" class="login" href="#login">Masuk</a></li>
				<li><a data-target="modal1" class="login" href="#daftar">Daftar</a></li>
				
				<!-- <li><a href="#!"><i class="small material-icons">store</i></a></li> -->
				
			</ul>
			
		    <div class="search-bar left">
				<form>
					<div class="input-field" style="background-color: #70A2F6;">
						<input type="search" id="search-field" class="field" placeholder="Explore" required maxlength="">
						<label for="search-field"><i class="small mdi-action-search"></i></label>
						<i class="mdi-navigation-close close"></i>
					</div>
				</form>
			</div>
			<ul id="slide-out" class="side-nav">
				<li><a href="#!"><i class="fa fa-opencart"></i> &nbsp; Express</a></li>
				<li><a href="#!"><i class="fa fa-industry"></i>  &nbsp; Suppliers </a></li>
				<li><a href="#!"><i class="fa fa-hand-paper-o"></i>  &nbsp; Deals</a></li>
				<li><div class="divider"></div></li>
			</ul>
			
			<a href="#" data-activates="slide-out" class="button-collapse"><i
				class="mdi-navigation-menu"></i></a>
			
		</nav>
	</div>
</header>

<div id="login" class="modal">
    <?php include 'login.php'?>
</div>


<div id="daftar" class="modal">
    <?php include 'register.php';?>
</div>

<a href="#0" class="cd-top">Top</a>