<div class="modal-content">
	<small>Kesemua kolom wajib di isi</small>
</div>
<div class="row">
	<form class="col s7" style="padding-left: 40px;">

		<div class="row">
			<div class="col s12">
				<input type="text" class="form-control" placeholder="Nama lengkap">
			</div>
			<div class="col s12">
				<input type="text" class="form-control" placeholder="Email">
			</div>
			<div class="col s12">
				<input type="password" class="form-control" placeholder="Password">
			</div>
			<div class="col s12">
				<input type="password" class="form-control"
					placeholder="Ulangi password">
			</div>
			<div class="col s12">
				<small>Dengan menekan daftar, Saya mengkonfirmasi kalau saya setuju
					dengan <a href="#">Kebijakan Privasi </a> di Tokotoku.com
				</small><br />
				<button class="btn btn-masuk waves-effect waves-light blue darken-2"
					style="margin-right: 10px;">Daftar</button>
			</div>
		</div>
	</form>

	<div class="col s5">
		<div class="row">
			<small>Daftar menggunakan : </small>
		</div>
		<div class="row" style="padding-left: 40px;">
			<button class="btn btn-facebook waves-effect waves-light"
				style="width: 150px;">Facebook</button>
		</div>
		<div class="row" style="padding-left: 40px;">
			<button class="btn btn-google waves-effect waves-light"
				style="width: 150px;">Google+</button>
		</div>
	</div>
</div>