<div id="footer">

  <div class="row">
    
      <div class="col s3">
        <div class="foot-header">
          <b>Bantuan</b> <img src="http://200.27.156.170/ean_default/img/cocha/icon-cocha.png">
        </div>
        <div class="foot-links">
          <a href="#">Sistem berbelanja</a>
          <a href="#">Penjual</a>
          <a href="#" target="_blank">Pembeli</a>
          <a href="#">Pembayaran</a>
          <a href="#">Pengiriman</a>
          <a href="#">Cek status pemesanan</a>
          <a href="#">Konfirmasi pembayaran</a>
          <a href="#">Peraturan</a>
         
        </div>
      </div><!--/col-sm-3-->
    <div class="col s3">
      <div class="foot-header"> Tokotoku.com <img src="http://200.27.156.170/ean_default/img/cocha/servicio-al-cliente-icon.png"></div>
      <div class="foot-links">
      	<a href="javascript:Contacto()">Tentang Tokotoku.com</a>
        <a href="#">Blog</a>
        <a href="#">Komunitas</a>
        <a href="#">Berita dan artikel</a>
        <a href="#">Karir</a>
      </div>
    </div>
    <div class="col s3">
      <div class="foot-header">
        Metode pembayaran <img src="http://200.27.156.170/ean_default/img/cocha/card-icon.png">
      </div>
      <div class="foot-links">
        <p>
          <i class="fa fa-check text-success"></i> Kartu Kredit<br>
          <img src="http://200.27.156.170/ean_default/img/cocha/visa-card.png">
          <img src="http://200.27.156.170/ean_default/img/cocha/master-card.png">
          <img src="http://200.27.156.170/ean_default/img/cocha/dinner-club-card.png">
        </p>
        <p>
          <i class="fa fa-check text-success"></i> Bank Transfer<br>
          <img src="http://200.27.156.170/ean_default/img/cocha/banco-santander-card-1.png">
          <img src="http://200.27.156.170/ean_default/img/cocha/banco-de-chile-card-1.png">
        </p>
        <p>
          <i class="fa fa-check text-success"></i> Internet Banking<br>
          <img src="https://my.dokuwallet.com/dwbo/images/dokuwallet-logo.png" style="width: 100px;">
        </p>
      </div>
    </div><!--/col-sm-3-->
    <div class="col s3">
      <img src="http://200.27.156.170/ean_default/img/cocha/tripadvisor-logo.jpg" class="img-responsive img-thumbnail">
      <br><br>
      <a href="#" style="float: left;">
        <img src="http://200.27.156.170/ean_default/img/cocha/sello_de_calidad_turistica.png" width="121" height="70" alt="sello de calidad turistica">
      </a>
      <a href="#" target="_blanck" style="float: left; margin-left: 20px;">
        <img src="http://200.27.156.170/ean_default/img/cocha/Logo_Chileestuyo.png" height="70" alt="Logo Chileestuyo">
      </a>
    </div>
    
  	</div><!--/row-->
  <div class="row">
    <center><img src="http://200.27.156.170/ean_default/img/cocha/footer-bg.png"></center>
    <div id="bottom-footer">
      <div class="row">
        <div class="col s12">
	    	<p><small>
	    		Tokotoku.com, Toko Online Terlengkap & Terpercaya Dengan Beragam Produk Pilihan Serta Promo Menarik
	    		</small>
			</p>
			<p>
				<small>
				Sebagai salah satu pelopor online mall di Indonesia, Tokotoku.com hadir dengan beragam produk pilihan yang disertai promo istimewa & menarik setiap hari. Hari Senin, Tokotoku.com memiliki promo MONDAY MOM'S DAY. Beragam produk spesial untuk ibu serta buah hati akan hadir dengan diskon istimewa, khusus di hari Senin mulai pukul 00.00 hingga 23.59 WIB. RABU CANTIK, promo spesial untuk Anda, wanita Indonesia yang ingin tampil stylish dan tentunya cantik. RABU CANTIK hadir setiap Rabu mulai pukul 00.00 hingga 23.59 WIB. Jangan lewatkan pula promo KAMIS GANTENG, 40 produk pilihan akan hadir ke hadapan Anda setiap hari Kamis, pukul 00.00 hingga 23.59 WIB. Tidak hanya itu, KAMIS GANTENG juga menghadirkan promo flash sale, dimana 5 produk mentereng dengan diskon gila-gilaan akan hadir bergantian selama 1 jam mulai pukul 10.00 WIB - 15.00 WIB. Nah, buat Anda yang hobi belanja saat weekend, jangan lewatkan promo FUN WEEKEND DEAL (FWD). Promo ini akan hadir ke hadapan Anda dengan tema berbeda dan diskon istimewa mulai hari Jumat pukul 15.00 WIB hingga hari Minggu pukul 23.59 WIB.
				Selain beragam promo dan deals menarik yang hadir setiap harinya, Tokotoku.com sebagai online shopping mall juga berkomitmen memberikan pengalaman berbelanja online shop yang aman, nyaman, mudah, menyenangkan, dimana saja, dan kapan saja. Nikmati belanja online produk terlengkap di Tokotoku.com, dengan didukung metode pembayaran yang aman disertai sertifikasi VeriSign dan Credit Card Fraud Detection System. Tersedia pula fasilitas cicilan 0% selama 6 dan 12 bulan dari beragam bank pilihan serta gratis pengiriman ke seluruh wilayah Indonesia. Tokotoku.com, Big Choices, Big Deals.
				</small> 
	    	</p>
        </div>
       
   	</div>
  </div><!--/row 2-->
      <div class="row" id="final-footer">
        <div class="col s4">
          <img src="http://200.27.156.170/ean_default/img/cocha/RapidSSL_SEAL-90x50.gif">
        </div>
        <div class="col s4 center">
        	Copyright &copy; 2015 Tokotoku.com All Rights Reserved <br />
			Jl. Kemanggisan raya, Jakarta Barat. Indonesia
        </div>
        <div class="col s4">
        	<span class="fa-stack fa-lg right"> <i
					class="fa fa-circle text-primary fa-stack-2x" style="color: #3B5998;"></i>
					<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
			</span>
          	<span class="fa-stack fa-lg right"> <i
				class="fa fa-circle text-info fa-stack-2x" style="color: #00ACEE;"></i>
				<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
			</span>
			<span class="fa-stack fa-lg right"> <i
					class="fa fa-circle fa-stack-2x" style="color: #517FA6;"></i> <i
					class="fa fa-instagram fa-stack-1x fa-inverse"></i>
			</span>
			<span class="fa-stack fa-lg right"> <i
					class="fa fa-circle fa-stack-2x" style="color: red;"></i> <i
					class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
			</span>
			<span class="fa-stack fa-lg right"> <i
					class="fa fa-circle fa-stack-2x" style="color: #0277bd;"></i> <i
					class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
			</span>
        </div>
      </div>
  
</div><!--/footer--></div>


<div id="push"></div>
        