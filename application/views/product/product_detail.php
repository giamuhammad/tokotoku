<script src="<?php echo $this->config->item('js')."simplegallery.min.js"; ?>"></script>	
<link href="<?php echo $this->config->item('css')."product.css"; ?>" rel="stylesheet">
<link href="<?php echo $this->config->item('css')."deskripsi.css"; ?>" rel="stylesheet">
<link href="<?php echo $this->config->item('css')."plugin/thumbnails.css"; ?>" rel="stylesheet">

<script type="text/javascript">
$(document).ready(function(){

    $('#gallery').simplegallery({
        galltime : 400,
        gallcontent: '.content',
        gallthumbnail: '.thumbnail',
        gallthumb: '.thumb'
    });

});
</script>
<div class="container-custom">
	
 	<div class="breadcrumbs">
 		<a class="breadcrumbs-link" href="#">Home</a> <i class="fa fa-angle-right"></i> 
 		<a class="breadcrumbs-link" href="#">Category</a>  <i class="fa fa-angle-right"></i> 
 		<a class="breadcrumbs-link" href="#">Sub Category</a> <i class="fa fa-angle-right"></i> 
 		<a class="breadcrumbs-link" href="#">Product Detail</a>
 	</div>
 	
 	
 	<section class="product">
		
		<div class="box effect2">
			<div class="title">Ps3 Slim Sony Hdd 120gb + Estenal 500giga Full Games</div>
			<div class="divider"></div>
			<div class="row">
				<!-- KOLOM 1 -->
				<div class="col s4 kolom1">
					<div id="gallery" class="simplegallery">
			            <div class="content">
			                <img src="https://s0.bukalapak.com/system4/images/3/9/4/2/5/7/7/0/medium/Cemara2L.jpg" class="image_1 materializebox" alt="" />
			                <img src="https://s3.bukalapak.com/system4/images/4/3/2/6/2/9/2/3/medium/mete_asli_wonogiri_super_5.jpg" class="image_2" style="display:none" alt="" />
			                <img src="https://s0.bukalapak.com/system4/images/2/4/8/0/1/5/8/0/medium/IMG_4684r_1.jpg" class="image_3" style="display:none" alt="" />
			                <img src="https://s0.bukalapak.com/system4/images/3/9/4/2/5/7/7/0/medium/Cemara2L.jpg" class="image_4" style="display:none" alt="" />
			            </div>
			
			            <div class="clear"></div>
			
			            <div class="thumbnail">
			                <div class="thumb">
			                    <a href="#" rel="1">
			                        <img src="https://s0.bukalapak.com/system4/images/3/9/4/2/5/7/7/0/medium/Cemara2L.jpg" id="thumb_1" alt=""  />
			                    </a>
			                </div>
			                <div class="thumb">
			                    <a href="#" rel="2">
			                        <img src="https://s3.bukalapak.com/system4/images/4/3/2/6/2/9/2/3/medium/mete_asli_wonogiri_super_5.jpg" id="thumb_2" alt="" />
			                    </a>
			                </div>
			                <div class="thumb">
			                    <a href="#" rel="3">
			                        <img src="https://s0.bukalapak.com/system4/images/2/4/8/0/1/5/8/0/medium/IMG_4684r_1.jpg" id="thumb_3" alt="" />
			                    </a>
			                </div>
			                <div class="thumb last">
			                    <a href="#" rel="4">
			                        <img src="https://s0.bukalapak.com/system4/images/3/9/4/2/5/7/7/0/medium/Cemara2L.jpg" id="thumb_4" alt="" />
			                    </a>
			                </div>
			            </div>
		      		</div>
	      		</div>
	      		
	      		<!-- KOLOM 2 -->
	      		<div class="col s4 kolom2">
					<!-- <div class="price"> Rp. 25.000,-</div> -->
					<div class="product_information">
						<div class="harga">Rp 2.000.000</div>
						<div class="stok tersedia"><b>10 STOK TERSEDIA</b></div>
						<div style="padding-top: 10px;">
							
						</div>
						
						<div class="quantity">
							<div class="input-field col s5">
								<select>
									<option value="1">1 QTY</option>
									<option value="2">2 QTY</option>
									<option value="3">3 QTY</option>
								</select>
    							<label>JUMLAH BARANG</label>
    						</div>
    						<div class="right">
    							<a data-target="modal1" href="#add_to_cart" class="waves-effect waves-light btn login" style="width: 100px; background-color: #4285F4;"><b>BELI</b></a>
    							
    						</div>
    						
    						<div class="input-field col s12">
								<select>
									<option value="1">Size 30</option>
									<option value="2">Size 31</option>
									<option value="3">Size 32</option>
								</select>
    							<label>PILIH UKURAN</label>
    						</div>
						</div>
						
						<div style="padding-top: 10px;">
							<small> <a href="#">Tanya Penjual</a></small> 
							<small>|</small> 
							<small> <a href="#">Ulasan produk dari <span style="border-bottom: 1px dotted #000;">10</span> Pembeli </a></small>
							<small>|</small>
							<small> <a href="#">Laporkan</a></small>
						</div>
						<div class="divider"></div>
						<div class="left" style="padding-top: 10px;">
					        	<?php echo $this->load->view('plugin/social_media');?>
						</div>
						
						
						
					</div>
	      		</div>
	      		<!-- KOLOM BLANK -->
	      		<div class="col s1">&nbsp;</div>
	      		<!-- KOLOM 3 -->
	      		<div class="col s3 kolom3">
	      			<div class="pengiriman">
	      				<b>Pilih Pengiriman</b> <i class="fa fa-map-o"></i>
	      				
						    <select class="browser-default provinsi">
						      <option value="" disabled selected>Pilih provinsi anda</option>
						      <option value="1">DKI Jakarta</option>
						      <option value="2">Jawa Barat</option>
						      <option value="3">Jawa Tengah</option>
						    </select>
						    
						    <select class="browser-default kota">
						      <option value="" disabled selected>Pilih kota anda</option>
						      <option value="1">Jakarta Barat</option>
						      <option value="2">Jakarta Pusat</option>
						      <option value="3">Jakarta Selatan</option>
						    </select>
						  
						   
						   
	      			</div>
	      			<div class="divider"></div>
	      			<div class="pengiriman-time">
	      				<b>Estimasi Penerimaan</b> <a href="#!"><i class="fa fa-question-circle" style="color: #777; vertical-align: baseline;"></i></a> 
	      				<div>
	      				<small>13 - 20 Agustus 2015</small>
	      				</div>
	      			</div>
	      			<div class="divider"></div>
	      			<div class="seller">
	      				<div>
	      					<b>Dijual & dikirim oleh </b>
	      				</div> 
	      				
	      				<table>
	      					<tr>
	      					<td>
		      					<small><a href="#">Macbeth</a></small>
		      					<!-- RATING -->
		      					<div style="color: #f57c00;">
			      					<small>
				      					<i class="fa fa-star"></i>
				      					<i class="fa fa-star"></i>
				      					<i class="fa fa-star"></i>
				      					<i class="fa fa-star-half-o"></i>
				      					<i class="fa fa-star-o"></i>
			      					</small>
			      				</div>
			      				<!-- FOTO AVATAR -->
			      				<div class="foto">
			      					<a href="#">
			      						<img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/v/t1.0-1/p160x160/248008_611717098856724_555199070_n.jpg?oh=88e3bd7682327a6461ff67fa3a07a9a6&oe=565B8019&__gda__=1448354157_be6e08f63aca6394b8e0c7b186fb3e6f" class="avatar">
			      					</a>
			      				</div>
			      				<!-- FEEDBACK -->
			      				<div>
			      					<small>100% Positive feedback</small>
			      				</div>
	      					</td>
	      					
	      					<td>
	      						<!-- REPUTASI -->
		      					<ul>
		      						<li><small>Reputasi : </small></li>
		      						<li> <small><i class="fa fa-check-square-o"></i> cepat </small></li>
		      						<li> <small><i class="fa fa-check-square-o"></i> terpercaya</small> </li>
		      						<li> <small><i class="fa fa-check-square-o"></i> ramah</small> </li>
		      						<li> <small><i class="fa fa-times"></i> responsive</small> </li>
		      					</ul>
	      					</td>
	      				</table>
	      				
	      			</div>
	      			
	      			
	      		</div>
			</div>	
		</div>
		
	</section>
 	
 	
 	<section class="box effect2">
 		<?php include_once 'deskripsi.php';?>
 	</section>
</div>


<!-- Modal Structure -->
<div id="add_to_cart" class="modal">
   <div class="modal-content">
     <h4>Modal Header</h4>
     <p>A bunch of text</p>
   </div>
   <div class="modal-footer">
     <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
   </div>
</div>
