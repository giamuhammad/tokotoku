<div class="row">
    <div class="col s7">
      <ul class="tabs">
        <li class="tab col s3"><a href="#tab1" class="active"  style="color: #4285F4;"><b>DESKRIPSI PRODUK</b></a></li>
        <li class="tab col s3"><a href="#test2"><b>FEEDBACK</b></a></li>
        <li class="tab col s4"><a href="#tab3"><b>PENGGUNAAN PRODUK</b></a></li>
      </ul>
    </div>
    
    <div id="tab1">
    	<div  class="col s12">
    		<div class="divider"></div>
    	</div>
    	<div  class="col s7" id="detail_barang">
	    	<table style="line-height: 1px;">
	    		<tr>
	    			<td width="5%;">
	    				<small>Kategori  </small>
	    			</td>
	    			<td width="2%;">:</td>
	    			<td>
	    				 <a href="#"><small>FOOD & BEVERAGE</small></a>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>
	    				<small>Kondisi  </small>
	    			</td>
	    			<td>:</td>
	    			<td>
	    				 <small class="kondisi" id="baru">BARU</small>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td>
	    				<small>Berat  </small>
	    			</td>
	    			<td>:</td>
	    			<td>
	    				 <small>400 gram</small>
	    			</td>
	    		</tr>
	    	</table>
	    	
	    	<div class="divider"></div>
	    	<div class="deskripsi">
	    		<p>
	    		Perangkat cctv menjadi pilihan yang ringkas dan praktis, selain itu kami menawarkan harga kamera cctv wireless terjangkau. Dapat diakses melalui gadget, dilengkapi 12 LED infrared. Mudah dipasang, gambar kualitas HD 300p dan slot micro sd. Kini Anda dapat berlibur dengan tenang tanpa terbeban tentang keamanan rumah.
	    		</p>
	    		<p>
				Gambar video berkualitas HD 300p
				Harga yang kami tawarkan memang tergolong murah, tetapi menghasilkan video dengan kualitas HD. Kemampuan yang tidak murahan dari perangkat murah. Dengan fitur tersebut, rekaman di siang maupun malam hari bisa terlihat jelas.
	    		</p>	
	    		<p>Fitur wireless mempermudah pemantauan
				Perangkat kamera ini memiliki fitur wireless sehingga membantu Anda pemantuan dari jarak jauh. Harga kamera cctv wireless yang terjangkau ini membuat Anda bisa memilikinya tanpa takut kemahalan. Dengan fitur ini pula Anda dapat melihat kondisi terkini rumah melalui gadget.
				</p>
				
				<p>
					Kelengkapan perangkat :<br />
					1 unit CCTV GSM<br />
					1 unit charger<br />
					1 batt lithium (standby 300 Jam)<br />
					1 Bracket<br />
				</p>
	    	</div>
	    </div>
    </div>
    <div id="test2" class="col s7">Test 2</div>
    
    <div id="tab3" class="col s12">
    	
    	<div class="divider"></div>
    	<div class="videoWrapper">
		    <!-- Copy & Pasted from YouTube -->
		    <iframe width="560" height="349" src="https://www.youtube.com/embed/KXSH13qXX2g" frameborder="0" allowfullscreen></iframe>
		</div>
    </div>
</div>