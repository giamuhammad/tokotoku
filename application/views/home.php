<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-US"><!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Tokotoku</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="<?php echo $this->config->item('assets')."materialize/css/materialize.css"; ?>" rel="stylesheet">
	<link href="<?php echo $this->config->item('assets')."materialize/css/materialize.css"; ?>" rel="stylesheet">
	<link href="<?php echo $this->config->item('css')."style.css"; ?>" rel="stylesheet">
	<script src="<?php echo $this->config->item('js')."jquery.min.js"; ?>"></script>
	<link href="<?php echo $this->config->item('css')."footer.css"; ?>" rel="stylesheet">
	<!-- Carousel -->
	<link rel="stylesheet" href="<?php echo $this->config->item('plugin')."carousel/owl.carousel.css"; ?>">
	<link rel="stylesheet" href="<?php echo $this->config->item('plugin')."carousel/owl.theme.css"; ?>">
</head>
<body>
	<?php include 'include/header.php';?>
	
	<?php switch($page) {
		case "main" : include 'plugin/carousel.php'; include 'container.php'; break;
		case "product_detail" : include 'product/product_detail.php'; break;
	}?>
	<div class="footer-upper">
   		<?php include "include/footer.php";?>
   	</div>
	
	
	
	<script src="<?php echo $this->config->item('js')."angular.min.js"; ?>"></script>	
	<script src="<?php echo $this->config->item('js')."common.js"; ?>"></script>	
	<script src="<?php echo $this->config->item('plugin')."carousel/owl.carousel.js"; ?>"></script>
	<script src="<?php echo $this->config->item('assets')."materialize/js/materialize.js"; ?>"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			
			$(".sideNav").click(function(){
				$('.button-collapse').sideNav('show');
			});
			$('select').material_select();
			$('.login, .daftar').leanModal();
			$('.materialboxed').materialbox();
			$("#fullslider").owlCarousel({
				 
			      navigation : true, // Show next and prev buttons
			      slideSpeed : 300,
			      paginationSpeed : 400,
			      singleItem:true,
			      navigation: false,
			      pagination: false,
			      autoPlay: 5000,
			      // "singleItem:true" is a shortcut for:
			      // items : 1, 
			      // itemsDesktop : false,
			      // itemsDesktopSmall : false,
			      itemsTablet: true,
			      itemsMobile : true
			 
			  });
			$(".button-collapse").sideNav();

			$(".card-title").click(function() {
				$(".more").slideToggle("fast");
			});

			
		});
	</script>
</body>
</html>