<a class="social_media" href="#"> <span class="fa-stack fa-lg"> <i
		class="fa fa-circle text-primary fa-stack-2x" style="color: #3B5998;"></i>
		<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
</span>
</a>
<a class="social_media" href="#"> <span class="fa-stack fa-lg"> <i
		class="fa fa-circle text-info fa-stack-2x" style="color: #00ACEE;"></i>
		<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
</span>
</a>

<a class="social_media" href="#"> <span class="fa-stack fa-lg"> <i
		class="fa fa-circle fa-stack-2x" style="color: #517FA6;"></i> <i
		class="fa fa-instagram fa-stack-1x fa-inverse"></i>
</span>
</a>
<a class="social_media" href="#"> <span class="fa-stack fa-lg"> <i
		class="fa fa-circle fa-stack-2x" style="color: red;"></i> <i
		class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
</span>
</a>
<a class="social_media" href="#"> <span class="fa-stack fa-lg"> <i
		class="fa fa-circle fa-stack-2x" style="color: #0277bd;"></i> <i
		class="fa fa-envelope-o fa-stack-1x fa-inverse"></i>
</span>
</a>