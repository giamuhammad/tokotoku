<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	
	function __construct(){
		parent::__construct();
		$this->auth();
	}

	public function index()
	{	
		$data['page'] = 'main';
		$this->load->view('home', $data);
	}
}
